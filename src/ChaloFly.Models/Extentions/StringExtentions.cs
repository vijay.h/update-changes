﻿// ***********************************************************************
// <copyright file="StringExtentions.cs" company="ChaloFly">
//     Copyright ©  2018
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Models.Extentions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="StringExtentions" />
    /// </summary>
    public static class StringExtentions
    {
        /// <summary>
        /// Converts string to the array item.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="delimiters">The delimiters.</param>
        /// <returns>The <see cref="List{ArrayItem{string}}"/></returns>
        //public static List<ArrayItem<string>> ToArrayItem(this string source, params string[] delimiters)
        //{
        //    var result = new List<ArrayItem<string>>();
        //    if (string.IsNullOrWhiteSpace(source))
        //    {
        //        return result;
        //    }

        //    var spliteItems = source.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        //    int index = 0;
        //    foreach (var splitItem in spliteItems.Where(x => !string.IsNullOrWhiteSpace(x)))
        //    {
        //        result.Add(new ArrayItem<string> { Key = ++index, Value = splitItem.Trim() });
        //    }

        //    return result;
        //}
    }
}
