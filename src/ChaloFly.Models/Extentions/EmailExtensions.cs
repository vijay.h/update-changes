﻿// ***********************************************************************
// <copyright file="EmailExtensions.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ChaloFly.Models.Extentions
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Mail;

    /// <summary>
    /// Email extensions class.
    /// </summary>
    public static class EmailExtensions
    {
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="emailTo">The email to.</param>
        /// <param name="password">The password.</param>
        /// <param name="isPasswordReset">if set to <c>true</c> [is password reset].</param>
        public static void SendEmail(string emailTo, string password, bool isPasswordReset)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["SMTP:From"]);
                    mail.To.Add(emailTo);                    
                    mail.Subject = isPasswordReset ? "ChaloFly Pest Management Mobile App Password" : "Notification";
                    mail.Body = isPasswordReset ? string.Format("<p>You requested your password for the ChaloFly Pest Management Mobile App.<br /><br />Your password is: {0}</p>", password) : "A new video has been added";
                    mail.IsBodyHtml = bool.Parse(ConfigurationManager.AppSettings["SMTP:IsHTMLBody"].ToString());

                    using (SmtpClient smtpClient = new SmtpClient())
                    {
                        smtpClient.Host = ConfigurationManager.AppSettings["SMTP:Server"].ToString();
                        smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTP:Port"].ToString());
                        smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTP:UserName"], ConfigurationManager.AppSettings["SMTP:Password"]);
                        smtpClient.EnableSsl = bool.Parse(ConfigurationManager.AppSettings["SMTP:EnableSsl"].ToString());
                        smtpClient.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
