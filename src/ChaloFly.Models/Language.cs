﻿// ***********************************************************************
// <copyright file="Language.cs" company="ChaloFly">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ChaloFly.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Model class for Language
    /// </summary>
    /// <seealso cref="ChaloFly.Models.BaseModel" />
    [Table("Language")]
    public class Language : BaseModel   
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [Key]
        public override int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }
    }
}
