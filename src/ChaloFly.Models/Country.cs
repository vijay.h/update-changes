﻿// ***********************************************************************
// <copyright file="Country.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;
    using Newtonsoft.Json;

    /// <summary>
    /// Model for country
    /// </summary>
    /// <seealso cref="ChaloFly.Models.BaseModel" />
    [Table("Country")]
    public class Country : BaseModel
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public override int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Remote("IsCountryExist", "Country", ErrorMessage = "country.exists", AdditionalFields = "Id")]
        [Required(ErrorMessage = "country.name.required")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>
        /// The country code.
        /// </value>
        [Remote("IsCountryCodeExist", "Country", ErrorMessage = "country.code.exists", AdditionalFields = "Id")]
        [Required(ErrorMessage = "country.code.required")]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the language identifier.
        /// </summary>
        /// <value>
        /// The language identifier.
        /// </value>
        [Required(ErrorMessage = "country.language.required")]
        [ForeignKey(nameof(Language))]
        public int LanguageId { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        [JsonIgnore]
        public virtual Language Language { get; set; }

        /// <summary>
        /// Assigns the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        public override void Assign(BaseModel source)
        {
            base.Assign(source);
            this.Name = (source as Country).Name;
            this.Code = (source as Country).Code;
            this.LanguageId = (source as Country).LanguageId;
        }
    }
}
