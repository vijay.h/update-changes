﻿// ***********************************************************************
// <copyright file="ErrorModel.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ************************************************************

namespace ChaloFly.Models
{
    using System;

    /// <summary>
    /// Error Model
    /// </summary>
    public class ErrorModel
    {
        /// <summary>
        /// Gets or sets the time stamp.
        /// </summary>
        /// <value>
        /// The time stamp.
        /// </value>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public object Data { get; set; }
    }
}
