﻿// ***********************************************************************
// <copyright file="Constants.cs" company="ChaloFly">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Models
{
    using System.Configuration;

    /// <summary>
    /// Defines the <see cref="Constants" />
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Gets the name of the BLOB storage application setting.
        /// </summary>
        /// <value>
        /// The name of the BLOB storage application setting.
        /// </value>
        public static string BlobStorageAppSettingName
        {
            get { return "azure:BlobStorageSAS"; }
        }

        /// <summary>
        /// Gets the name of the storage container.
        /// </summary>
        /// <value>
        /// The name of the storage container.
        /// </value>
        public static string StorageContainerName
        {
            get { return ConfigurationManager.AppSettings["azure:StorageContainerName"]; }
        }

        /// <summary>
        /// Gets the name of the documents bundle.
        /// </summary>
        /// <value>
        /// The name of the documents bundle.
        /// </value>
        public static string DocumnetsBundleName
        {
            get { return "countrydocuments.zip"; }
        }

        /// <summary>
        /// Gets the name of the images bundle.
        /// </summary>
        /// <value>
        /// The name of the images bundle.
        /// </value>
        public static string ImagesBundleName
        {
            get { return "images.zip"; }
        }

        /// <summary>
        /// Gets the customer base URL.
        /// </summary>
        /// <value>
        /// The product images path.
        /// </value>
        public static string ProductImagesPath
        {
            get { return "productimages"; }
        }

        /// <summary>
        /// Gets the product label documents path.
        /// </summary>
        /// <value>
        /// The product label documents path.
        /// </value>
        public static string ProductLabelDocumentsPath
        {
            get { return "labeldocuments"; }
        }

        /// <summary>
        /// Gets the green trust lm image path.
        /// </summary>
        /// <value>
        /// The green trust lm image path.
        /// </value>
        public static string GreenTrustIconPath
        {
            get { return "greentrusticons"; }
        }

        /// <summary>
        /// Gets the green trust documents path.
        /// </summary>
        /// <value>
        /// The green trust documents path.
        /// </value>
        public static string GreenTrustDocumentsPath
        {
            get { return "greentrustdocuments"; }
        }

        /// <summary>
        /// Gets the product special label documents path.
        /// </summary>
        /// <value>
        /// The product special label documents path.
        /// </value>
        public static string ProductSpecialLabelDocumentsPath
        {
            get { return "speciallabeldocuments"; }
        }

        /// <summary>
        /// Gets the pest images path.
        /// </summary>
        /// <value>
        /// The pest images path.
        /// </value>
        public static string PestImagesPath
        {
            get { return "pestimages"; }
        }

        /// <summary>
        /// Gets the secure choice documents path.
        /// </summary>
        /// <value>
        /// The secure choice documents path.
        /// </value>
        public static string SecureChoiceDocumentsPath
        {
            get { return "securechoicedocuments"; }
        }

        /// <summary>
        /// Gets the secure choice assurance images path.
        /// </summary>
        /// <value>
        /// The secure choice assurance images path.
        /// </value>
        public static string SecureChoiceImagesPath
        {
            get { return "securechoiceimages"; }
        }

        /// <summary>
        /// Gets the product temporary documents path.
        /// </summary>
        /// <value>
        /// The product temporary documents path.
        /// </value>
        public static string ProductTempDocumentsPath
        {
            get { return "tempdocumentspath"; }
        }

        /// <summary>
        /// Gets the PPM videos documents path.
        /// </summary>
        /// <value>
        /// The PPM videos documents path.
        /// </value>
        public static string PPMVideosPath
        {
            get { return "ppmvideos"; }
        }

        /// <summary>
        /// Gets the PPM videos thumbnails path.
        /// </summary>
        /// <value>
        /// The PPM videos thumbnails path.
        /// </value>
        public static string PPMVideosThumbnailsPath
        {
            get { return "ppmvideoThumbnails"; }
        }

        /// <summary>
        /// Gets the name of the country content bundle.
        /// </summary>
        /// <value>
        /// The name of the country content bundle.
        /// </value>
        public static string CountryContentBundleName
        {
            get { return "countrycontent.zip"; }
        }

        /// <summary>
        /// Gets the agronomic programs documents path.
        /// </summary>
        /// <value>
        /// The agronomic programs documents path.
        /// </value>
        public static string AgronomicProgramsDocumentsPath
        {
            get { return "agronomicprogramsdocuments"; }
        }

        /// <summary>
        /// Gets the green cast PDF link.
        /// </summary>
        /// <value>
        /// The green cast PDF link.
        /// </value>
        public static string GreenCastPdfLink
        {
            get { return "http://www.greencastonline.com/ornamental/programs/"; }
        }
    }
}
