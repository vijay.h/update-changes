﻿// ***********************************************************************
// <copyright file="BaseModel.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Models
{
    using System;

    /// <summary>
    /// Base Model for all the classes
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseModel"/> class.
        /// </summary>
        public BaseModel()
        {
           this.CreatedOn = DateTime.Now;
           this.ModifiedOn = DateTime.Now;
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is active; otherwise, <c>false</c>.
        /// </value>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is deleted; otherwise, <c>false</c>.
        /// </value>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>
        /// The created on.
        /// </value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public int ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified on.
        /// </summary>
        /// <value>
        /// The modified on.
        /// </value>
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Assigns the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        public virtual void Assign(BaseModel source)
        {
            this.CreatedBy = source.CreatedBy;
            this.CreatedOn = source.CreatedOn;
            this.IsActive = source.IsActive;
            this.IsDeleted = source.IsDeleted;
            this.ModifiedBy = source.ModifiedBy;
            this.ModifiedOn = source.ModifiedOn;
        }

        /// <summary>
        /// Gets the last update time.
        /// </summary>
        /// <returns>Last Update Time</returns>
        public virtual DateTime GetLastUpdateTime()
        {
            return this.ModifiedOn;
        }
    }
}
