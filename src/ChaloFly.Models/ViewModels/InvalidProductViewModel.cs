﻿using System;
using System.Collections.Generic;
// ***********************************************************************
// <copyright file="InvalidProductViewModel.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Models.ViewModels
{
    /// <summary>
    /// The Invalid Product View Model
    /// </summary>
    public class InvalidProductViewModel
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the scan location.
        /// </summary>
        /// <value>
        /// The scan location.
        /// </value>
        public string ScanLocation { get; set; }

        /// <summary>
        /// Gets or sets the response message.
        /// </summary>
        /// <value>
        /// The response message.
        /// </value>
        public string ResponseMessage { get; set; }

        /// <summary>
        /// Gets or sets the geo location.
        /// </summary>
        /// <value>
        /// The geo location.
        /// </value>
        public string GeoLocation { get; set; }
    }
}
