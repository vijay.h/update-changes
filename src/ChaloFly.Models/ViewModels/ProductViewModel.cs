﻿// ***********************************************************************
// <copyright file="ProductViewModel.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Models.ViewModels
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The Product View Model
    /// </summary>
    public class ProductViewModel
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user email.
        /// </summary>
        /// <value>
        /// The user email.
        /// </value>
        public string UserEmail { get; set; }

        /// <summary>
        /// Gets or sets the scan location.
        /// </summary>
        /// <value>
        /// The scan location.
        /// </value>
        public string ScanLocation { get; set; }

        /// <summary>
        /// Gets or sets the scan number.
        /// </summary>
        /// <value>
        /// The scan number.
        /// </value>
        public int ScanNumber { get; set; }

        /// <summary>
        /// Gets or sets the gtin.
        /// </summary>
        /// <value>
        /// The gtin.
        /// </value>
        public string GTIN { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>
        /// The serial number.
        /// </value>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        /// <value>
        /// The batch number.
        /// </value>
        public string BatchNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the geo location.
        /// </summary>
        /// <value>
        /// The geo location.
        /// </value>
        public string GeoLocation { get; set; }

        /// <summary>
        /// Gets or sets the type of the response message.
        /// </summary>
        /// <value>
        /// The type of the response message.
        /// </value>
        public int ResponseMessageType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is geniune.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is geniune; otherwise, <c>false</c>.
        /// </value>
        public bool IsGeniune { get; set; }

        /// <summary>
        /// Gets or sets the manufacture date.
        /// </summary>
        /// <value>
        /// The manufacture date.
        /// </value>
        public string ManufactureDate { get; set; }

        /// <summary>
        /// Gets or sets the expiry date.
        /// </summary>
        /// <value>
        /// The expiry date.
        /// </value>
        public string ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the manufacture at.
        /// </summary>
        /// <value>
        /// The manufacture at.
        /// </value>
        public string ManufactureAt { get; set; }

        /// <summary>
        /// Gets or sets the formulation.
        /// </summary>
        /// <value>
        /// The formulation.
        /// </value>
        public string RiskLevel { get; set; }

        /// <summary>
        /// Gets or sets the filled and packed.
        /// </summary>
        /// <value>
        /// The filled and packed.
        /// </value>
        public string FilledAndPacked { get; set; }

        /// <summary>
        /// Gets or sets the images.
        /// </summary>
        /// <value>
        /// The images.
        /// </value>
        public List<string> Images { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        public string Comments { get; set; }
    }
}
