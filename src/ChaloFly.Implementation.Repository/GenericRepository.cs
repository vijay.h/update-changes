﻿// ***********************************************************************
// <copyright file="GenericRepository.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Implementation.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using ChaloFly.Interface.Repository;
    using ChaloFly.Models;

    /// <summary>
    /// Generic repository to communicate with Database
    /// </summary>
    /// <typeparam name="T">Entity object</typeparam>
    /// <seealso cref="ChaloFly.Interface.Repository.IGenericRepository{T}" />
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseModel
    {
        /// <summary>
        /// The context
        /// </summary>
        private DbSet<T> objectSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public GenericRepository(IChaloContext context)
        {
            this.objectSet = context.Set<T>();
        }

        /// <summary>
        /// Gets or sets the object set.
        /// </summary>
        /// <value>
        /// The object set.
        /// </value>
        public DbSet<T> ObjectSet
        {
            get { return this.objectSet; }
            set { this.objectSet = value; }
        }
        
        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="enitity">The Entity.</param>
        /// <returns>
        /// return id inserted
        /// </returns>
        public T InsertOrUpdate(T enitity)
        {
            var existingModel = this.GetById(enitity.Id);
            if (existingModel != null)
            {
                existingModel.Assign(enitity);
            }
            else
            {
                this.objectSet.Add(enitity);
            }

            return enitity;
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="includeExpressions">list of entities to be included</param>
        /// <returns>
        /// returns entity by id
        /// </returns>
        public T GetById(object id, params Expression<Func<T, object>>[] includeExpressions)
        {
            var intID = Convert.ToInt32(id);
            var query = this.objectSet.AsQueryable();
            if (includeExpressions != null)
            {
                foreach (var includeExpression in includeExpressions)
                {
                    query = query.Include(includeExpression);
                }
            }

            return query.FirstOrDefault(x => x.Id == intID);
        }

        /// <summary>
        /// Determines whether [is duplicate entity exists] [the specified identifier].
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [is duplicate entity exists] [the specified identifier]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDuplicateEntityExists(int id, string value)
        {
            return this.objectSet.Any(x => x.Id != id && x.IsDeleted == false);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="includeExpressions">The include expressions.</param>
        /// <returns>
        /// gets list of objects
        /// </returns>
        public IQueryable<T> GetAll(params Expression<Func<T, object>>[] includeExpressions)
        {
            var query = this.objectSet.AsQueryable();
            if (includeExpressions != null)
            {
                foreach (var includeExpression in includeExpressions)
                {
                    query = query.Include(includeExpression);
                }
            }

            return query;
        }

        /// <summary>
        /// Queries the specified filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="includeExpressions">The include expressions.</param>
        /// <returns>
        /// returns list of objects by condition
        /// </returns>
        public IEnumerable<T> Query(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] includeExpressions)
        {
            var query = this.objectSet.AsQueryable();
            if (includeExpressions != null)
            {
                foreach (var includeExpression in includeExpressions)
                {
                    query = query.Include(includeExpression);
                }
            }

            return query.Where(filter);
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Add(T entity)
        {
            this.objectSet.Add(entity);
        }

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="entities">The entities.</param>
        public void AddRange(List<T> entities)
        {
            this.objectSet.AddRange(entities);
        }

        /// <summary>
        /// Removes the range.
        /// </summary>
        /// <param name="entities">The entities.</param>
        public void RemoveRange(List<T> entities)
        {
            this.objectSet.RemoveRange(entities);
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>true or false</returns>
        public bool Update(T entity)
        {
            return true;
        }

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true or false
        /// </returns>
        public bool Remove(T entity)
        {
            bool isDeleted = false;
            var existingModel = this.GetById(entity.Id);
            if (existingModel != null)
            {
                existingModel.ModifiedBy = entity.ModifiedBy;
                existingModel.ModifiedOn = entity.ModifiedOn;
                existingModel.IsDeleted = true;
                isDeleted = true;
            }

            return isDeleted;
        }
    }
}
