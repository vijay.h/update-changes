﻿// ***********************************************************************
// <copyright file="ChaloFlyContext.cs" company="ChaloFly">
//     Copyright ? ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ChaloFly.Implementation.Repository
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using ChaloFly.Interface.Repository;
    using ChaloFly.Models;

    /// <summary>
    /// ChaloFlyContext initialize context
    /// </summary>
    /// <seealso cref="ChaloFly.Interface.Repository.IChaloContext" />
    /// <seealso cref="System.Data.Entity.DbContext" />
    public class ChaloContext : DbContext, IChaloContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChaloContext" /> class.
        /// </summary>
        public ChaloContext() : base("name=DBConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        /// <datetime>2/11/2016 2:20 PM</datetime>
        public IDbConnection Connection
        {
            get { return Database.Connection; }
        }
        
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public DbSet<Country> Country { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        public DbSet<Language> Language { get; set; }

        /// <summary>
        /// Gets or sets Emailmodel
        /// </summary>
        public void Save()
        {
            this.SaveChanges();
        }

        /// <summary>
        /// Executes the stored procedure.
        /// </summary>
        /// <typeparam name="T">Generic types</typeparam>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="parms">The parameters.</param>
        /// <returns>
        /// generic list of result
        /// </returns>
        public virtual IList<T> ExecuteStoredProcedure<T>(string procedureName, List<SqlParameter> parms)
        {
            var result = default(IList<T>);

            var sqlQuery = new StringBuilder(string.Format("EXECUTE {0} ", procedureName));
            var parmArray = default(SqlParameter[]);

            if (parms != null && parms.Count > 0)
            {
                for (int i = 0; i < parms.ToArray().Length; i++)
                {
                    sqlQuery.Append(string.Format("{0} {1},", parms[i].ParameterName, parms[i].Direction == ParameterDirection.Input ? string.Empty : "out"));
                }

                sqlQuery = sqlQuery.Remove(sqlQuery.Length - 1, 1);
            }

            if (parms == null || parms.Count == 0)
            {
                result = this.Database.SqlQuery<T>(sqlQuery.ToString()).ToList();
            }
            else
            {
                parmArray = parms.ToArray();
                result = this.Database.SqlQuery<T>(sqlQuery.ToString(), parmArray).ToList();
            }

            if (parmArray != null)
            {
                parms = parmArray.ToList();
            }

            return result;
        }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ChaloContext>(null);
            base.OnModelCreating(modelBuilder);
            bool isWebAPI = bool.Parse(ConfigurationManager.AppSettings["IsWebAPI"]);
            if (isWebAPI)
            {
                modelBuilder.Entity<TestModel>().ToTable("Test", "dbo");
                modelBuilder.Entity<Country>().ToTable("Country", "dbo");
            }
            else
            {
                //// Product Tables
               //// modelBuilder.Entity<Product>().ToTable("Product", "dbo");
            }
        }
    }
}