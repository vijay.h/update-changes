﻿// ***********************************************************************
// <copyright file="UnitOfWork.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Implementation.Repository
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Threading;
    using System.Web;
    using ChaloFly.Interface.Repository;
    using ChaloFly.Models;

    /// <summary>
    /// Initialize the unit of work
    /// </summary>
    /// <seealso cref="ChaloFly.Turkey.Interface.Repository.IUnitOfWork" />
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Defines the HTTPCONTEXTKEY
        /// </summary>
        private const string HTTPCONTEXTKEY = "PrimaryObjects.Repository.Base.HttpContext.Key";

        /// <summary>
        /// Defines the Threads
        /// </summary>
        private static readonly Hashtable Threads = new Hashtable();

        /// <summary>
        /// Defines the context
        /// </summary>
        private IChaloContext context;

        /// <summary>
        /// The country repository
        /// </summary>
        private IGenericRepository<Country> countryRepository;

        /// <summary>
        /// The language repository
        /// </summary>
        private IGenericRepository<Language> languageRepository;

        /// <summary>
        /// The test repository
        /// </summary>
        private IGenericRepository<TestModel> testRepository;


        /// <summary>
        /// Defines the disposedValue
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <exception cref="ArgumentNullException">Context was not supplied</exception>
        public UnitOfWork(IChaloContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets the Current
        /// </summary>
        public static IUnitOfWork Current
        {
            get
            {
                IUnitOfWork unitOfWork = GetUnitOfWork();
                if (unitOfWork == null)
                {
                    unitOfWork = new UnitOfWork(new ChaloContext());
                    SaveUnitOfWork(unitOfWork);
                }

                return unitOfWork;
            }
        }

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public IChaloContext Context
        {
            get { return this.context; }
            set { this.context = value; }
        }

        /// <summary>
        /// Gets the country repository.
        /// </summary>
        /// <value>
        /// The country repository.
        /// </value>
        public IGenericRepository<Country> CountryRepository
        {
            get
            {
                if (this.countryRepository == null)
                {
                    this.countryRepository = new GenericRepository<Country>(this.context);
                }

                return this.countryRepository;
            }
        }

        /// <summary>
        /// Gets the language repository.
        /// </summary>
        /// <value>
        /// The language repository.
        /// </value>
        public IGenericRepository<Language> LanguageRepository
        {
            get
            {
                if (this.languageRepository == null)
                {
                    this.languageRepository = new GenericRepository<Language>(this.context);
                }

                return this.languageRepository;
            }
        }

        /// <summary>
        /// Gets or sets the test repository.
        /// </summary>
        /// <value>
        /// The test repository.
        /// </value>
        public IGenericRepository<TestModel> TestRepository
        {
            get
            {
                if (this.testRepository == null)
                {
                    this.testRepository = new GenericRepository<TestModel>(this.context);
                }

                return this.testRepository;
            }
        }

        /// <summary>
        /// The Dispose
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            this.context.Dispose();
        }

        /// <summary>
        /// The Save
        /// </summary>
        public void Save()
        {
            this.context.Configuration.ValidateOnSaveEnabled = false;
            this.context.Save();
        }

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        public void Rollback()
        {
            this.context.Dispose();
            this.context = new ChaloContext();
        }

        /// <summary>
        /// Detaches the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Detach(BaseModel entity)
        {
            this.context.Entry(entity).State = EntityState.Detached;
        }

        /// <summary>
        /// Executes the procedure.
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="parameters">The parameters.</param>
        public void ExecuteProcedure(string procedureName, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString))
            {
                using (var command = new SqlCommand(procedureName, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null && parameters.Length > 0)
                    {
                        command.Parameters.AddRange(parameters);
                    }

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Executes the procedure.
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// Data set
        /// </returns>
        public DataSet ExecuteProcedureForGet(string procedureName, params SqlParameter[] parameters)
        {
            DataSet data;
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString))
            {
                using (var command = new SqlCommand(procedureName, connection))
                {
                    connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null && parameters.Length > 0)
                    {
                        command.Parameters.AddRange(parameters);
                    }

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = command;
                    DataSet ds = new DataSet();
                    da.Fill(ds, command.ToString());
                    data = ds;
                }
            }

            return data;
        }

        /// <summary>
        /// The Dispose
        /// </summary>
        /// <param name="disposing">The disposing<see cref="bool"/></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // <TODO>: free unmanaged resources (unmanaged objects) and override a finalizer below. &  set large fields to null.
                this.disposedValue = true;
            }
        }

        /// <summary>
        /// The GetUnitOfWork
        /// </summary>
        /// <returns>The <see cref="IUnitOfWork"/></returns>
        private static IUnitOfWork GetUnitOfWork()
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items.Contains(HTTPCONTEXTKEY))
                {
                    return (IUnitOfWork)HttpContext.Current.Items[HTTPCONTEXTKEY];
                }

                return null;
            }
            else
            {
                Thread thread = Thread.CurrentThread;
                if (string.IsNullOrEmpty(thread.Name))
                {
                    thread.Name = Guid.NewGuid().ToString();
                    return null;
                }
                else
                {
                    lock (Threads.SyncRoot)
                    {
                        return (IUnitOfWork)Threads[Thread.CurrentThread.Name];
                    }
                }
            }
        }

        /// <summary>
        /// The SaveUnitOfWork
        /// </summary>
        /// <param name="unitOfWork">The unitOfWork<see cref="IUnitOfWork"/></param>
        private static void SaveUnitOfWork(IUnitOfWork unitOfWork)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items[HTTPCONTEXTKEY] = unitOfWork;
            }
            else
            {
                lock (Threads.SyncRoot)
                {
                    Threads[Thread.CurrentThread.Name] = unitOfWork;
                }
            }
        }
    }
}
