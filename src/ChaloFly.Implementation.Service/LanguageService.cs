﻿// ***********************************************************************
// <copyright file="LanguageService.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// *********************************************************************

namespace ChaloFly.Implementation.Service
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using ChaloFly.Interface.Repository;
    using ChaloFly.Interface.Service;
    using ChaloFly.Models;

    /// <summary>
    /// Language Service
    /// </summary>
    /// <seealso cref="ChaloFly.Implementation.Service.BaseService{ChaloFly.Models.Language}" />
    /// <seealso cref="ChaloFly.Interface.Service.ILanguageService" />
    public class LanguageService : BaseService<Language>, ILanguageService
    {
        #region Variables

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageService" /> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        public LanguageService(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            this.Repository = this.UnitOfWork.LanguageRepository;
        }

        #endregion

        /// <summary>
        /// Gets all models.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.List`1" />
        /// </returns>
        public override List<Language> GetAll(int skip = 0, int take = 500)
        {
            ////var result = base.GetAll(skip, take);
            var result = this.Repository.GetAll().Where(x => x.IsActive == true && x.IsDeleted == false).OrderByDescending(x => x.Id).Skip(skip).Take(take);
            return result.OrderBy(x => x.Name).ToList();
        }

        /// <summary>
        /// Gets model the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The <see cref="!:T" />
        /// </returns>
        public override Language GetById(int id)
        {
            return this.Repository.GetById(id);
        }

        /// <summary>
        /// Gets the by code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>
        /// Instance of language by it's code
        /// </returns>
        public Language GetByCode(string code)
        {
            return this.Repository.Query(x => x.Code == code).FirstOrDefault();
        }

        /// <summary>
        /// Gets the by language identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Instance of language by it's id
        /// </returns>
        public Language GetByLanguageId(int id)
        {
            return this.Repository.GetAll().Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <returns>List of Languages</returns>
        public List<Language> GetLanguages()
        {
            return this.Repository.GetAll().Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
        }
    }
}
