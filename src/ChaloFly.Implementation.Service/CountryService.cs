﻿// ***********************************************************************
// <copyright file="CountryService.cs" company="ChaloFly">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Implementation.Service
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using ChaloFly.Interface.Repository;
    using ChaloFly.Interface.Service;
    using ChaloFly.Models;

    /// <summary>
    /// Country Service
    /// </summary>
    /// <seealso cref="ChaloFly.Implementation.Service.BaseService{ChaloFly.Models.Country}" />
    /// <seealso cref="ChaloFly.Interface.Service.ICountryService" />
    public class CountryService : BaseService<Country>, ICountryService
    {
        #region Variables
        /// <summary>
        /// Initializes a new instance of the <see cref="CountryService"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        public CountryService(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            this.Repository = this.UnitOfWork.CountryRepository;
        }
        #endregion

        /// <summary>
        /// Gets all models.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>
        /// The <see cref="T:System.Collections.Generic.List`1" />
        /// </returns>
        public override List<Country> GetAll(int skip = 0, int take = 500)
        {
            ////var result = base.GetAll(skip, take);
            var result = this.Repository.GetAll().Include(e => e.Language).OrderByDescending(x => x.Id).Skip(skip).Take(take);
            return result.OrderBy(x => x.Name).ToList();
        }

        /// <summary>
        /// Gets the by code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>
        /// instance of country
        /// </returns>
        public Country GetByCode(string code)
        {
            return this.UnitOfWork.CountryRepository.Query(x => x.Code == code).FirstOrDefault();
        }

        /// <summary>
        /// Gets all country codes.
        /// </summary>
        /// <returns>
        /// Gets all countries | Delimited
        /// </returns>
        public string GetAllCountryCodes()
        {
            string str = string.Empty;
            var countryCodes = this.UnitOfWork.CountryRepository.GetAll().Select(c => c.Code).ToList();
            str = string.Join("|", countryCodes);
            return str;
        }

        /// <summary>
        /// Gets the country identifier by code.
        /// </summary>
        /// <param name="countryCode">The country code.</param>
        /// <returns>
        /// Get Country Id By Code
        /// </returns>
        public int GetCountryIdByCode(string countryCode)
        {
            var country = this.UnitOfWork.CountryRepository.GetAll().Where(c => c.Code == countryCode).FirstOrDefault();
            return country != null ? country.Id : default(int);
        }

        /// <summary>
        /// Gets the country by code.
        /// </summary>
        /// <param name="countryCode">The country code.</param>
        /// <returns>
        /// Get Country details
        /// </returns>
        public Country GetCountryByCode(string countryCode)
        {
            return this.UnitOfWork.CountryRepository.GetAll().Where(c => c.Code == countryCode).FirstOrDefault();
        }

        /// <summary>
        /// Gets the by country identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The country
        /// </returns>
        public Country GetByCountryId(int id)
        {
            ////var country = this.UnitOfWork.CountryRepository.GetById(id);
            var country = this.UnitOfWork.CountryRepository.GetAll().Where(i => i.Id == id).FirstOrDefault();
            return country;
        }
    }
}
