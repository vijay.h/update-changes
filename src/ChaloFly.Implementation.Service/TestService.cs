﻿using ChaloFly.Interface.Repository;
using ChaloFly.Interface.Service;
using ChaloFly.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaloFly.Implementation.Service
{
    public class TestService : BaseService<TestModel>, ITestService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestService"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        public TestService(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            this.Repository = this.UnitOfWork.TestRepository;
        }

        public IQueryable<TestModel> GetTestData(int id)
        {
            return this.Repository.GetAll().Where(x => x.Id == id);
        }

        public TestModel SaveTestData(TestModel TestModel)
        {
            //this.Repository.Add(TestModel);
            var data = this.Repository.InsertOrUpdate(TestModel);
            this.UnitOfWork.Save();
            return data;
        }
    }
}
