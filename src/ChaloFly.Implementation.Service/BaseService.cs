﻿// ***********************************************************************
// <copyright file="BaseService.cs" company="ChaloFly">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Implementation.Service
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;
    using ChaloFly.Interface.Repository;
    using ChaloFly.Interface.Service;
    using ChaloFly.Models;

    /// <summary>
    /// Defines the <see cref="BaseService" />
    /// </summary>
    /// <typeparam name="T">type of parameter <see cref="T" /></typeparam>
    public abstract class BaseService<T> : IBaseService<T> where T : BaseModel
    {
        /// <summary>
        /// The disposed value
        /// </summary>
        private bool disposedValue = false; 

        /// <summary>
        /// Gets or sets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        protected IUnitOfWork UnitOfWork { get; set; }

        /// <summary>
        /// Gets or sets the Repository
        /// </summary>
        protected IGenericRepository<T> Repository { get; set; }

        /// <summary>
        /// Gets all models.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>The <see cref="List{T}"/></returns>
        public virtual List<T> GetAll(int skip = 0, int take = 500)
        {
            return this.Repository.GetAll().Where(x => x.IsDeleted == false).OrderByDescending(x => x.Id)
                .Skip(0).Take(take).ToList();
        }

        /// <summary>
        /// Gets the models by country identifier.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>The <see cref="List{T}"/></returns>
        public virtual List<T> GetByCountryId(int countryId, int skip = 0, int take = 500)
        {
            return this.GetAll(skip, take);
        }

        /// <summary>
        /// Gets model the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The <see cref="T"/></returns>
        public virtual T GetById(int id)
        {
            return this.Repository.GetById(id);
        }

        /// <summary>
        /// Removes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        public void Remove(T model)
        {
            this.Repository.Remove(model);
            this.UnitOfWork.Save();
        }

        /// <summary>
        /// Saves the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        public virtual void Save(T model)
        {
            this.Repository.InsertOrUpdate(model);
            this.UnitOfWork.Save();
        }

        #region IDisposable Support
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                this.UnitOfWork.Dispose();
                this.disposedValue = true;
            }
        }

        /// <summary>
        /// Converts the data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        protected virtual List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr">The dr.</param>
        /// <returns></returns>
        protected virtual T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return obj;
        }
        #endregion
    }
}
