﻿// ***********************************************************************
// <copyright file="Global.asax.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;
    using ChaloFly.Interface.Service;
    using Unity;

    /// <summary>
    /// Web API Application
    /// </summary>
    /// <seealso cref="System.Web.HttpApplication" />
    public class WebApiApplication : System.Web.HttpApplication
    {
        /////// <summary>
        /////// The common service
        /////// </summary>
        ////private ICountryService commonService;

        /////// <summary>
        /////// Initializes a new instance of the <see cref="WebApiApplication"/> class.
        /////// </summary>
        ////public WebApiApplication()
        ////{
        ////    this.commonService = UnityConfig.Container.Resolve<ICountryService>();
        ////}

        /// <summary>
        /// Applications the start.
        /// </summary>
        protected void Application_Start()
        {
            //UnityMvcActivator.Start();
            //AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
