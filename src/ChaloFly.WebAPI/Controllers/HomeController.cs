﻿// ***********************************************************************
// <copyright file="HomeController.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.Controllers
{
    using System.Reflection;
    using System.Web.Mvc;

    /// <summary>
    /// Home Controller 
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class HomeController : Controller
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>index view</returns>
        public ActionResult Index()
        {
            return this.Content("Web API Started.  v " + Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }
    }
}
