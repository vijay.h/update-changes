﻿// ***********************************************************************
// <copyright file="BaseController.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.Controllers
{
    using System.Web.Http;
    using ChaloFly.Interface.Repository;
    using ChaloFly.Interface.Service;
    using ChaloFly.Models;
    using Unity;
    using Unity.Resolution;

    /// <summary>
    /// Base Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class BaseController : ApiController
    {
        /// <summary>
        /// The country
        /// </summary>
        private Country country = null;

        /// <summary>
        /// The country service
        /// </summary>
        private ICountryService countryService;

        /// <summary>
        /// The language service
        /// </summary>
        private ILanguageService languageService;

        /// <summary>
        /// The unit of work
        /// </summary>
        private IUnitOfWork unitOfWork;

        /// <summary>
        /// The storage service
        /// </summary>
        private ITestService testService;

        /// <summary>
        /// Gets the country identifier.
        /// </summary>
        /// <value>
        /// The country identifier.
        /// </value>
        public int CountryId
        {
            get { return this.Country?.Id ?? 0; }
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        public IUnitOfWork UnitOfWork
        {
            get { return this.unitOfWork ?? (this.unitOfWork = UnityConfig.Container.Resolve<IUnitOfWork>()); }
        }

        /// <summary>
        /// Gets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public Country Country
        {
            get
            {
                if (this.country != null)
                {
                    return this.country;
                }

                string countryCode = this.RequestContext.RouteData.Values["countrycode"].ToString();
                return this.country = this.CountryService.GetByCode(countryCode);
            }
        }

        /// <summary>
        /// Gets the country service.
        /// </summary>
        /// <value>
        /// The country service.
        /// </value>
        public ICountryService CountryService
        {
            get { return this.ResolveService(ref this.countryService); }
        }

        /// <summary>
        /// Gets the language service.
        /// </summary>
        /// <value>
        /// The language service.
        /// </value>
        public ILanguageService LanguageService
        {
            get { return this.ResolveService(ref this.languageService); }
        }

        /// <summary>
        /// Gets the tes service.
        /// </summary>
        /// <value>
        /// The tes service.
        /// </value>
        public ITestService TesService
        {
            get { return this.testService ?? (this.testService = UnityConfig.Container.Resolve<ITestService>()); }
        }


        /// <summary>
        /// Resolves the service.
        /// </summary>
        /// <typeparam name="T">type of object</typeparam>
        /// <param name="interfaceObject">The interface object.</param>
        /// <returns>
        /// Resolved object
        /// </returns>
        protected T ResolveService<T>(ref T interfaceObject) where T : class
        {
            return interfaceObject ?? (interfaceObject = UnityConfig.Container.Resolve<T>(new ParameterOverride("unitOfWork", this.UnitOfWork)));
        }
    }
}
