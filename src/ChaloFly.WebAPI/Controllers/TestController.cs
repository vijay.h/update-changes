﻿// ***********************************************************************
// <copyright file="ProductController.cs" company="ChaloFly">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.Controllers
{
    using ChaloFly.Models;
    using Swashbuckle.Swagger.Annotations;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    /// <summary>
    /// Test Controller
    /// </summary>
    /// <seealso cref="ChaloFly.WebAPI.Controllers.BaseController" />
    public class TestController : BaseController
    {
        /// <summary>
        /// Saves the test dat.
        /// </summary>
        /// <param name="testModel">The test model.</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(TestModel))]
        public HttpResponseMessage SaveTestDat(TestModel testModel)
        {
            if (testModel != null)
            {
                testModel.CreatedBy = testModel.ModifiedBy = 1;
                testModel.CreatedOn = testModel.ModifiedOn = DateTime.UtcNow;
                testModel.IsActive = true;
                testModel.IsDeleted = false;
            }
            var data = this.TesService.SaveTestData(testModel);

            if(data.Id > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Gets the test data.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(TestModel))]
        public HttpResponseMessage GetTestData(int id)
        {
            var data = this.TesService.GetTestData(id).FirstOrDefault();

            if (data != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }

    }
}
