﻿// ***********************************************************************
// <copyright file="Startup.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************

[assembly: Microsoft.Owin.OwinStartup(typeof(ChaloFly.WebAPI.App_Start.Startup))]

namespace ChaloFly.WebAPI.App_Start
{
    using Owin;

    /// <summary>
    /// Start up.cs
    /// </summary>
    public partial class Startup
    {
        #region Public Methods
        /// <summary>
        /// Configurations the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureAuth(app);
        }
        #endregion Public Methods
    }
}