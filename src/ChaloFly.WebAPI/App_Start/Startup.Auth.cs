﻿// ***********************************************************************
// <copyright file="Startup.Auth.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.App_Start
{
    using System;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.OAuth;
    using Owin;
    using ChaloFly.WebAPI.Security;

    /// <summary>
    /// Start up 
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// Gets the o authentication options.
        /// </summary>
        /// <value>
        /// The o authentication options.
        /// </value>
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        #region Public Methods
        /// <summary>
        /// Configures the authentication.
        /// </summary>
        /// <param name="app">The application.</param>
        public void ConfigureAuth(IAppBuilder app)
        {
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(),
                AuthorizeEndpointPath = new PathString("/api/Account/EndPoint"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(10), ////read this from web.config
                AllowInsecureHttp = true //// In production mode set AllowInsecureHttp = false
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
        }
        #endregion Public Methods
    }
}