﻿// ***********************************************************************
// <copyright file="WebApiConfig.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI
{
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Web.Http;
    using Microsoft.Owin.Security.OAuth;
    using Newtonsoft.Json;
    using ChaloFly.Interface.Service;
    using ChaloFly.WebAPI.Security;
    using Unity;

    /// <summary>
    /// Web API Configuration class
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Registers the specified configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void Register(HttpConfiguration config)
        {           
            ICountryService commonService;
            commonService = UnityConfig.Container.Resolve<ICountryService>();
            string countryCodes = commonService.GetAllCountryCodes();
            config.MapHttpAttributeRoutes();

            config.Filters.Add(new ExceptionHandler());

            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
           
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            ////config.Routes.MapHttpRoute(
            ////    name: "AccountApi",
            ////    routeTemplate: "api/{countryCode}/Account/{action}/{id}/{id2}/{id3}",
            ////    defaults: new { controller = "Account", id = RouteParameter.Optional, id2 = RouteParameter.Optional, id3 = RouteParameter.Optional },
            ////    constraints: new { countryCode = "Global|" + countryCodes });

            ////config.Routes.MapHttpRoute(
            ////    name: "MasterApi",
            ////    routeTemplate: "api/{countryCode}/Master/{action}/{id}/{id2}/{id3}",
            ////    defaults: new { controller = "Master", id = RouteParameter.Optional, id2 = RouteParameter.Optional, id3 = RouteParameter.Optional },
            ////    constraints: new { countryCode = "Global|" + countryCodes });

            ////config.Routes.MapHttpRoute(
            ////    name: "TranslationApi",
            ////    routeTemplate: "api/{countryCode}/Translation/{action}/{id}/{id2}/{id3}",
            ////    defaults: new { controller = "Translation", id = RouteParameter.Optional, id2 = RouteParameter.Optional, id3 = RouteParameter.Optional },
            ////    constraints: new { countryCode = "Global|" + countryCodes });

            ////config.Routes.MapHttpRoute(
            ////    name: "ClientWebJobApi",
            ////    routeTemplate: "api/{countryCode}/ClientWebJob/{action}/{id}/{id2}/{id3}",
            ////    defaults: new { controller = "ClientWebJob", id = RouteParameter.Optional, id2 = RouteParameter.Optional, id3 = RouteParameter.Optional },
            ////    constraints: new { countryCode = "Global|" + countryCodes });

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                ////routeTemplate: "api/{countryCode}/{controller}/{action}/{id}",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional, id2 = RouteParameter.Optional, id3 = RouteParameter.Optional }
                ////constraints: new { countryCode = countryCodes }
                );
        }
    }
}
