﻿// ***********************************************************************
// <copyright file="WebApiAuthorize.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.Security
{
    using System.Web.Http;
    using System.Web.Http.Controllers;

    /// <summary>
    /// Web API Authorize
    /// </summary>
    /// <seealso cref="System.Web.Http.AuthorizeAttribute" />
    public class WebApiAuthorize : AuthorizeAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebApiAuthorize"/> class.
        /// </summary>
        public WebApiAuthorize()
        {
            ////this.appUserSessionService = UnityConfig.Container.Resolve<IAppUserSessionService>();
        }

        /// <summary>
        /// Indicates whether the specified control is authorized.
        /// </summary>
        /// <param name="actionContext">The context.</param>
        /// <returns>
        /// true if the control is authorized; otherwise, false.
        /// </returns>
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var result = base.IsAuthorized(actionContext);
            if (!result)
            {
                return false;
            }
            ////var user = ApplicationOAuthProvider.GetUserToken();
            ////if (user == null)
            ////{
            ////    return false;
            ////}

            ////if (!this.appUserSessionService.IsValidSession(user.EmailAddress, user.Token))
            ////{
            ////    return false;
            ////}

            return true;
        }
    }
}