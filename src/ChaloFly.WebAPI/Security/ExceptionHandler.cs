﻿// ***********************************************************************
// <copyright file="ExceptionHandler.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.Security
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Filters;
    using NLog;
    using ChaloFly.Models;

    /// <summary>
    /// Exception handler class.
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.ExceptionFilterAttribute" />
    public class ExceptionHandler : ExceptionFilterAttribute
    {
        /// <summary>
        /// The logger
        /// </summary>
        private Logger logger = LogManager.GetLogger("ChaloFly.WebAPI");

        /// <summary>
        /// Called when [exception].
        /// </summary>
        /// <param name="context">The context.</param>
        public async override void OnException(HttpActionExecutedContext context)
        {
            var uniqueId = Guid.NewGuid();
            string requestData = string.Empty;

            try
            {
                requestData = await context.Request.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                requestData = "Coudn't read post data";
            }
            //// Get stack trace for the exception with source file information
            var st = new StackTrace(context.Exception, true);
            //// Get the top stack frame
            var frame = st.GetFrame(0);
            //// Get the line number from the stack frame
            var line = frame.GetFileLineNumber();
            GlobalDiagnosticsContext.Set("UniqueId", uniqueId.ToString());
            GlobalDiagnosticsContext.Set("PostBody", requestData);
            GlobalDiagnosticsContext.Set("LineNumber", line);
            this.logger.Log(LogLevel.Error, context.Exception);
            base.OnException(context);
            context.Response = context.Request.CreateResponse(
                HttpStatusCode.InternalServerError,
                new ErrorModel
                {
                    Message = "Unexpected error occured. Reference Id : " + uniqueId
                });
        }
    }
}