﻿// ***********************************************************************
// <copyright file="ApplicationOAuthProvider.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ChaloFly.WebAPI.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.OAuth;
    using ChaloFly.Interface.Service;
    using ChaloFly.Models;
    using ChaloFly.WebAPI.App_Start;

    /// <summary>
    /// Application O Authentication Provider class
    /// </summary>
    /// <seealso cref="Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerProvider" />
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        #region Public Methods
        
        ///// <summary>
        ///// Signs the in.
        ///// </summary>
        ///// <param name="appSessionService">The application session service.</param>
        ///// <param name="appUser">State of the patient.</param>
        ///// <returns>
        ///// The authorization token.
        ///// </returns>
        ////public static LoginResponse SignIn(IAppUserSessionService appSessionService, string appUser)
        ////{
        ////    var claims = new List<Claim>
        ////            {
        ////                new Claim(ClaimTypes.Name, appUser),
        ////                new Claim("userState", appUser.SerializeToXML())
        ////            };

        ////    var identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
        ////    var props = new AuthenticationProperties()
        ////    {
        ////        IssuedUtc = DateTime.UtcNow,
        ////        ExpiresUtc = DateTime.UtcNow.AddYears(10)
        ////    };
        ////    var ticket = new AuthenticationTicket(identity, props);
        ////    var token = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
        ////    var session = new AppUserSession
        ////    {
        ////        UserName = appUser,
        ////        AuthToken = token,
        ////        IssuedDate = props.IssuedUtc.Value.DateTime,
        ////        ExpiryDate = props.ExpiresUtc.Value.DateTime,
        ////        IsActive = true,
        ////        IsDeleted = false
        ////    };

        ////    appSessionService.Save(session);
        ////    return new LoginResponse { AccessToken = string.Format("Bearer {0}", token), ErrorCode = string.Empty, IsSuccess = true };
        ////}

        ///// <summary>
        ///// Gets the patient.
        ///// </summary>
        ///// <returns>
        ///// The patient details.
        ///// </returns>
        ////public static AppUser GetUserToken()
        ////{
        ////    var appUser = (HttpContext.Current.Items["MS_HttpRequestMessage"] as HttpRequestMessage).GetOwinContext().Authentication.User.Claims.Where(x => x.Type == "userState").FirstOrDefault();
        ////    if (appUser == null)
        ////    {
        ////        return null;
        ////    }

        ////    var userContext = appUser.Value.DeserializeFromXML<string>();
        ////    var accessToken = HttpContext.Current.Request.Headers.GetValues("Authorization");
        ////    if (accessToken == null || accessToken.Length < 1)
        ////    {
        ////        return null;
        ////    }

        ////    AppUser user = new AppUser
        ////    {
        ////        EmailAddress = userContext,
        ////        Token = accessToken[0].Substring(7)
        ////    };

        ////    return user;
        ////}

        ///// <summary>
        ///// Refreshes the token.
        ///// </summary>
        ///// <param name="appSessionService">The application session service.</param>
        ///// <returns>
        ///// New authorization token.
        ///// </returns>
        ////public static LoginResponse RefreshToken(IAppUserSessionService appSessionService)
        ////{
        ////    var appUser = GetUserToken();
        ////    if (appUser == null)
        ////    {
        ////        return null;
        ////    }

        ////    if (!appSessionService.IsValidSession(appUser.EmailAddress, appUser.Token))
        ////    {
        ////        return null;
        ////    }

        ////    appSessionService.RemoveAuthToken(appUser.EmailAddress);
        ////    return SignIn(appSessionService, appUser.EmailAddress);
        ////}

        /// <summary>
        /// Called at the final stage of a successful Token endpoint request. An application may implement this call in order to do any final
        /// modification of the claims being used to issue access or refresh tokens. This call may also be used in order to add additional
        /// response parameters to the Token endpoint's j son response body.
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// Called to validate that the origin of the request is a registered "client_id", and that the correct credentials for that client are
        /// present on the request. If the web application accepts Basic authentication credentials,
        /// context.TryGetBasicCredentials(out clientId, out clientSecret) may be called to acquire those values if present in the request header. If the web
        /// application accepts "client_id" and "client_secret" as form encoded POST parameters,
        /// context.TryGetFormCredentials(out clientId, out clientSecret) may be called to acquire those values if present in the request body.
        /// If context.Validated is not called the request will not proceed further.
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }
        #endregion Public Methods
    }
}