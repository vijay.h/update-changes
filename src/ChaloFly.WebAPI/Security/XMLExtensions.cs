﻿// ***********************************************************************
// <copyright file="XMLExtensions.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ChaloFly.WebAPI.Security
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// XML Extensions class.
    /// </summary>
    public static class XMLExtensions
    {
        #region Public Methods

        /// <summary>
        /// Serializes to XML.
        /// </summary>
        /// <typeparam name="T">The generic.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The string.
        /// </returns>
        /// <exception cref="Exception">An error occurred</exception>
        public static string SerializeToXML<T>(this T value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                var stringWriter = new StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, value);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }

        /// <summary>
        /// Deserializes from XML.
        /// </summary>
        /// <typeparam name="T">The generic.</typeparam>
        /// <param name="xml">The XML.</param>
        /// <returns>
        /// The generic result.
        /// </returns>
        /// <exception cref="Exception">An error occurred</exception>
        public static T DeserializeFromXML<T>(this string xml)
        {
            if (string.IsNullOrWhiteSpace(xml))
            {
                return default(T);
            }

            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                var reader = new StringReader(xml);
                T result = (T)xmlserializer.Deserialize(reader);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }

        #endregion Public Methods
    }
}