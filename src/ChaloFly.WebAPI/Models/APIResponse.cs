﻿// ***********************************************************************
// <copyright file="APIResponse.cs" company="ChaloFly">
//     Copyright � ChaloFly 2019
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.Models
{
    /// <summary>
    /// API response model
    /// </summary>
    /// <typeparam name="T">The generic</typeparam>
    public class APIResponse<T>
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public T Data { get; set; }
    }
}