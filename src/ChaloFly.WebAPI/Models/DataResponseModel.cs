﻿// ***********************************************************************
// <copyright file="DataResponseModel.cs" company="ChaloFly">
//     Copyright ©  2018
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.WebAPI.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="DataResponseModel{T1, T2}" />
    /// </summary>
    /// <typeparam name="T1">Type of parameter 1</typeparam>
    /// <typeparam name="T2">Type of parameter 2</typeparam>
    public class DataResponseModel<T1, T2> where T2 : class
    {
        /// <summary>
        /// Gets or sets the active entities.
        /// </summary>
        /// <value>
        /// The active entities.
        /// </value>
        public List<T1> ActiveEntities { get; set; } = new List<T1>();

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public T2 Result { get; set; }
    }
}
