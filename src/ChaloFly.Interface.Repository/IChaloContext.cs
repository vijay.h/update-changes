﻿// ***********************************************************************
// <copyright file="IChaloFlyContext.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Interface.Repository
{
    using System;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// context interface
    /// </summary>
    public interface IChaloContext : IDisposable
    {
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        DbContextConfiguration Configuration { get; }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        /// <author>santosh</author><datetime>2/11/2016 2:20 PM</datetime>
        IDbConnection Connection { get; }

        /// <summary>
        /// The Save
        /// </summary>
        void Save();

        /// <summary>
        /// Sets this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>sets the entity</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Entries the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns> Generic Entity</returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
