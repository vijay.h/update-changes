﻿// ***********************************************************************
// <copyright file="IUnitOfWork.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Interface.Repository
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using ChaloFly.Models;

    /// <summary>
    /// Interface for unit of work
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        IChaloContext Context { get; set; }

        /// <summary>
        /// Gets the country repository.
        /// </summary>
        /// <value>
        /// The country repository.
        /// </value>
        IGenericRepository<Country> CountryRepository { get; }

        /// <summary>
        /// Gets the language repository.
        /// </summary>
        /// <value>
        /// The language repository.
        /// </value>
        IGenericRepository<Language> LanguageRepository { get; }

        /// <summary>
        /// Gets or sets the test repository.
        /// </summary>
        /// <value>
        /// The test repository.
        /// </value>
        IGenericRepository<TestModel> TestRepository { get; }

        /// <summary>
        /// Save the context
        /// </summary>
        void Save();

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        void Rollback();

        /// <summary>
        /// Detaches the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Detach(BaseModel entity);

        /// <summary>
        /// Executes the procedure.
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="parameters">The parameters.</param>
        void ExecuteProcedure(string procedureName, params SqlParameter[] parameters);

        /// <summary>
        /// Executes the procedure for get.
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>Data table</returns>
        DataSet ExecuteProcedureForGet(string procedureName, params SqlParameter[] parameters);
    }
}
