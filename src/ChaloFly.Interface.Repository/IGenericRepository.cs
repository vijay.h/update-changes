﻿// ***********************************************************************
// <copyright file="IGenericRepository.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Interface.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    
    /// <summary>
    /// Initialize interface
    /// </summary>
    /// <typeparam name="T">entity name</typeparam>
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Gets or sets the object set.
        /// </summary>
        /// <value>
        /// The object set.
        /// </value>
        DbSet<T> ObjectSet { get; set; }

        /// <summary>
        /// Inserts the or update.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>returns id of inserted record</returns>
        T InsertOrUpdate(T entity);

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="includeExpressions">The include expressions.</param>
        /// <returns>
        /// returns entity
        /// </returns>
        T GetById(object id, params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="includeExpressions">The include expressions.</param>
        /// <returns>
        /// Gets list
        /// </returns>
        IQueryable<T> GetAll(params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Queries the specified filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="includeExpressions">The include expressions.</param>
        /// <returns>
        /// returns list of objects by condition
        /// </returns>
        IEnumerable<T> Query(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] includeExpressions);
       
        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Add(T entity);

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="entities">The entities.</param>
        void AddRange(List<T> entities);

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="entities">The entities.</param>
        void RemoveRange(List<T> entities);

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true or false
        /// </returns>
        bool Remove(T entity);
    }
}
