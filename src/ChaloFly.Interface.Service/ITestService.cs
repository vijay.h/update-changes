﻿// ***********************************************************************
// <copyright file="TestService.cs" company="ChaloFly">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Interface.Service
{
    using ChaloFly.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// ITest Service
    /// </summary>
    /// <seealso cref="ChaloFly.Interface.Service.IBaseService{ChaloFly.Models.TestModel}" />
    public interface ITestService : IBaseService<TestModel>
    {
        /// <summary>
        /// Saves the test data.
        /// </summary>
        /// <param name="TestModel">The test model.</param>
        /// <returns></returns>
        TestModel SaveTestData(TestModel TestModel);

        /// <summary>
        /// Gets the test data.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        IQueryable<TestModel> GetTestData(int id);
    }
}
