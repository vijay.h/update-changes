﻿// ***********************************************************************
// <copyright file="ILanguageService.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Interface.Service
{
    using System.Collections.Generic;
    using ChaloFly.Models;

    /// <summary>
    /// ILanguage Service
    /// </summary>
    public interface ILanguageService : IBaseService<Language>
    {
        /// <summary>
        /// Gets the by code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>
        /// Instance of language by it's code
        /// </returns>
        Language GetByCode(string code);

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <returns>Return List</returns>
        List<Language> GetLanguages();
    }
}
