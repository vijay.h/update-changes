﻿// ***********************************************************************
// <copyright file="IBaseService.cs" company="ChaloFly">
//     Copyright ©  2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Interface.Service
{
    using System;
    using System.Collections.Generic;
    using ChaloFly.Models;

    /// <summary>
    /// Defines the <see cref="IBaseService{T}" />
    /// </summary>
    /// <typeparam name="T">type of parameter <see cref="T" /></typeparam>
    public interface IBaseService<T> : IDisposable where T : BaseModel
    {
        /// <summary>
        /// Gets all models.
        /// </summary>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>
        /// Returns all the models
        /// </returns>
        List<T> GetAll(int skip = 0, int take = 500);

        /// <summary>
        /// Gets the models by country identifier.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>
        /// Returns the model by country identifier
        /// </returns>
        List<T> GetByCountryId(int countryId, int skip = 0, int take = 500);

        /// <summary>
        /// Gets model the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Returns the model by identifier
        /// </returns>
        T GetById(int id);

        /// <summary>
        /// Removes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        void Remove(T model);

        /// <summary>
        /// Saves the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        void Save(T model);
    }
}
