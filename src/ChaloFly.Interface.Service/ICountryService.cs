﻿// ***********************************************************************
// <copyright file="ICountryService.cs" company="ChaloFly">
//     Copyright � ChaloFly 2020
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace ChaloFly.Interface.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using ChaloFly.Models;

    /// <summary>
    /// Country Service Interface
    /// </summary>
    public interface ICountryService : IBaseService<Country>
    {
        /// <summary>
        /// Gets the by code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>
        /// instance of country
        /// </returns>
        Country GetByCode(string code);

        /// <summary>
        /// Gets all country codes.
        /// </summary>
        /// <returns>
        /// Get Delimited Country Codes
        /// </returns>
        string GetAllCountryCodes();

        /// <summary>
        /// Gets the country identifier by code.
        /// </summary>
        /// <param name="countryCode">The country code.</param>
        /// <returns>country by is</returns>
        int GetCountryIdByCode(string countryCode);

        /// <summary>
        /// Gets the country by code.
        /// </summary>
        /// <param name="countryCode">The country code.</param>
        /// <returns>Get Country details</returns>
        Country GetCountryByCode(string countryCode);

        /// <summary>
        /// Gets the by country identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The country</returns>
        Country GetByCountryId(int id);        
    }
}
